﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace SecurityLabs
{
    public partial class LogInForm : Form
    {
        public enum CheckStatus
        {
            Error,
            Level1,
            Level2,
            Level3,
            Level4
        }


        private DataBaseAccess _db;

        public LogInForm()
        {
            _db = new DataBaseAccess();

            InitializeComponent();
        }


        private static void PaintBackground(Control control, Color first,
            Color second, Single angle, PaintEventArgs e)
        {
            Rectangle r = new Rectangle(control.Location, control.Size);
            using ( var brush = 
                new LinearGradientBrush(r, first, second, angle) ) 
            {
                e.Graphics.FillRectangle(brush, r);
            }
        }


        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            PaintBackground(topPanel,
                Color.DodgerBlue, Color.SteelBlue, 90F,
                e);

            PaintBackground(contentPanel,
                Color.AliceBlue, Color.GhostWhite, 90F,
                e);

            PaintBackground(bottomPanel,
                Color.AliceBlue, Color.GhostWhite, 90F,
                e);
        }


        private void LogInForm_Resize(object sender, EventArgs e)
        {
//            topPanel.Invalidate();
//            contentPanel.Invalidate();
//            bottomPanel.Invalidate();

            this.Invalidate();
        }


        private void LogInForm_Load(object sender, EventArgs e)
        {
            int height = topPanel.Height + minLogInPanel.Height +
                         bottomPanel.Height;

            this.ClientSize = new Size(this.ClientSize.Width, height);
        }


        private void paramsButton_Click(object sender, EventArgs e)
        {
            if ( minLogInPanel.Visible ) {
                minLogInPanel.Visible = false;
                maxLogInPanel.Visible = true;
                int height = topPanel.Height + maxLogInPanel.Height +
                             bottomPanel.Height;
                this.ClientSize = new Size(this.ClientSize.Width, height);
                parButton.Text = "Вход <<";
                logInButton.Text = "Регистрация";
            } else {
                minLogInPanel.Visible = true;
                maxLogInPanel.Visible = false;
                int height = topPanel.Height + minLogInPanel.Height +
                             bottomPanel.Height;
                this.ClientSize = new Size(this.ClientSize.Width, height);
                parButton.Text = "Регистрация >>";
                logInButton.Text = "Вход";
            }
        }


        private void logInButton_Click(object sender, EventArgs e)
        {
            if ( minLogInPanel.Visible ) {
                String login = minLoginTextBox.Text;
                String pswd = minPswdTextBox.Text;

                if ( !_db.Auth(login, pswd) ) {
                    MessageBox.Show("Логин или пароль не верны!", "Упсии!");
                    return;
                } else {
                    MessageBox.Show("Все отлично! Вы вошли!", "Угу!");
                    return;
                }

            } else {
                String msg;
                String pswd = maxRepeatPswdTextBox.Text;
                if ( CheckPswd(pswd, out msg) == CheckStatus.Error ) {
                    MessageBox.Show(msg, "Ошибка!");
                    return;
                }

                String login = maxLoginTextBox.Text;
                if ( _db.ContainsLogin(login) ) {
                    MessageBox.Show("Пользователь с таким логином уже сущесвтует", "Ошибка!");
                    return;
                }

                String firstName = firstNameTextBox.Text;
                if ( firstNameTextBox.Text == String.Empty ) {
                    MessageBox.Show("Имя не может быть пустым", "Ошибка!");
                    return;
                }

                String secondName = secondNameTextBox.Text;
                if ( secondNameTextBox.Text == String.Empty ) {
                    MessageBox.Show("Фамилия не может быть пустой", "Ошибка!");
                    return;
                }

                if ( !_db.RegisterUser(firstName, secondName, login, pswd) ) {
                    MessageBox.Show("Что-то пошло не так", "Ошибка!");
                    return;
                } else {
                    MessageBox.Show("Поздравления! Вы смогли зарегистрироваться!", "Ура-ура!");
                    return;
                }
            }
        }


        private CheckStatus CheckLevel(String text, out String output)
        {
            Regex regex = new Regex("[A-ZА-Я]");
            bool upperCase = regex.IsMatch(text);

            regex = new Regex("[^A-Za-zа-яА-Я0-9_]");
            bool symbols = regex.IsMatch(text);

            regex = new Regex("[А-Яа-я]");
            bool russian = regex.IsMatch(text);

            regex = new Regex("[A-Za-z]");
            bool english = regex.IsMatch(text);

            bool both = russian & english;


            if ( upperCase & both & symbols ) {
                output = "Ваш пароль просто божественен!";
                return CheckStatus.Level4;
            }

            if ( upperCase & both ) {
                output = "Ваш пароль прекрасен, но можно еще лушче!\n" +
                    "Добавте немного символов вроде : . /";
                return CheckStatus.Level3;
            }
            if ( upperCase & symbols ) {
                output = "Ваш пароль прекрасен, но можно еще лушче!\n" +
                    "Добавте немного символов из английского или русского";
                return CheckStatus.Level3;
            }
            if ( symbols & both ) {
                output = "Ваш пароль прекрасен, но можно еще лушче!\n" + 
                    "Добавте немного символов из разных регистров";
                return CheckStatus.Level3;
            }


            if ( !upperCase && !both && !symbols ) {
                output = "Пойдет, но стоит подумать о безопасности!\n" +
                    "Добавте чего-нибудь вроде символов из разных регистров или каки-то таких : . /";
                return CheckStatus.Level1;
            } else {
                if ( !upperCase ) {
                    output = "Пойдет, но стоит подумать о безопасности!\n" +
                        "Добавте немного символов из разных регистров";
                    return CheckStatus.Level2;
                }
                else if ( !both ) {
                    output = "Пойдет, но стоит подумать о безопасности!\n" +
                        "Добавте немного символов из английского или русского";
                    return CheckStatus.Level2;
                }
                else {
                    output = "Пойдет, но стоит подумать о безопасности!\n" +
                        "Добавте немного символов из разных регистров или английского или русского";
                    return CheckStatus.Level2;
                }
            }
        }


        private CheckStatus CheckPswd(String text, out String output)
        {
            if ( text.Length < 8 ) {
                output = "Пароль должен сосотоять хотя бы из 8 символов";
                return CheckStatus.Error;
            }

            Regex regex = new Regex("[A-Za-zА-Яа-я]");
            if ( !regex.IsMatch(text) ) {
                output = "Пароль должен содержать по крайней мере один символ";
                return CheckStatus.Error;
            }

            regex = new Regex("[0-9]");
            if ( !regex.IsMatch(text) ) {
                output = "Пароль должен содержать по крайней мере одину цифру";
                return CheckStatus.Error;
            }
            
            return CheckLevel(text, out output);
        }


        private void CheckMaxLoginTextBox()
        {
            if ( maxLoginTextBox.Text == String.Empty ) {
                maxLoginCheckLabel.Text = "Не может быть пустым!";
                maxLoginCheckLabel.Visible = true;
                maxLoginCheckLabel.ForeColor = Color.Red;
            }
        }


        private void CheckMaxPswdTextBox()
        {
            if ( maxPswdTextBox.Text == String.Empty ) {
                maxPswdCheckLabel.Text = "Не может быть пустым!";
                maxPswdCheckLabel.Visible = true;
                maxPswdCheckLabel.ForeColor = Color.Red;
            }
        }


        private void MaxRepeatPswdTextBoxTextBox()
        {
            if ( maxRepeatPswdTextBox.Text == String.Empty ) {
                repeatPasswordCheckLabel.Text = "Не может быть пустым!";
                repeatPasswordCheckLabel.Visible = true;
                repeatPasswordCheckLabel.ForeColor = Color.Red;
            }
        }


        private void maxLoginTextBox_TextChanged(object sender, EventArgs e)
        {
            CheckFirstSecondName();

            if ( _db.ContainsLogin(maxLoginTextBox.Text) ) {
                maxLoginCheckLabel.Text = "Уже существует!";
                maxLoginCheckLabel.Visible = true;
                maxLoginCheckLabel.ForeColor = Color.Red;
            } else {
                maxLoginCheckLabel.Text = "Все прекрасно!";
                maxLoginCheckLabel.Visible = true;
                maxLoginCheckLabel.ForeColor = Color.Green;
            }
        }


        private void maxPswdTextBox_TextChanged(object sender, EventArgs e)
        {
            CheckFirstSecondName();
            CheckMaxLoginTextBox();

            String res;
            CheckStatus status = CheckPswd(maxPswdTextBox.Text, out res);
            if ( status == CheckStatus.Error ) {
                maxPswdCheckLabel.ForeColor = Color.Red;
                maxPswdCheckLabel.Text = "Ошибка!";
                maxPswdCheckLabel.Font = new Font(maxPswdCheckLabel.Font, FontStyle.Underline);
            } else if ( status == CheckStatus.Level3 ) {
                maxPswdCheckLabel.ForeColor = Color.Green;
                maxPswdCheckLabel.Text = "Убедительно! Уровень 3";
                maxPswdCheckLabel.Font = new Font(maxPswdCheckLabel.Font, FontStyle.Underline);
            } else if ( status == CheckStatus.Level2 ) {
                maxPswdCheckLabel.ForeColor = Color.YellowGreen;
                maxPswdCheckLabel.Text = "Предупреждение! Уровень 2";
                maxPswdCheckLabel.Font = new Font(maxPswdCheckLabel.Font, FontStyle.Underline);
            } else if ( status == CheckStatus.Level1 ) {
                maxPswdCheckLabel.ForeColor = Color.Orange;
                maxPswdCheckLabel.Text = "Предупреждение! Уровень 1";
                maxPswdCheckLabel.Font = new Font(maxPswdCheckLabel.Font, FontStyle.Underline);
            } else {
                maxPswdCheckLabel.ForeColor = Color.DarkGreen;
                maxPswdCheckLabel.Text = "Превосходно! Уровень 4";
                maxPswdCheckLabel.Font = new Font(maxPswdCheckLabel.Font, FontStyle.Underline);
            }

            maxPswdCheckLabel.Visible = true;
            toolTip1.SetToolTip(maxPswdCheckLabel, res);


            if ( maxRepeatPswdTextBox.Text != maxPswdTextBox.Text ) {
                repeatPasswordCheckLabel.Text = "Не совпадают!";
                repeatPasswordCheckLabel.Visible = true;
                repeatPasswordCheckLabel.ForeColor = Color.Red;
            } else {
                repeatPasswordCheckLabel.Text = "Совпадают!";
                repeatPasswordCheckLabel.Visible = true;
                repeatPasswordCheckLabel.ForeColor = Color.Green;
            }
        }


        private void CheckFirstSecondName()
        {
            if ( firstNameTextBox.Text == String.Empty ) {
                firstNameCheckLabel.Visible = true;
                firstNameCheckLabel.Text = "Не может быть пустым!";
                firstNameCheckLabel.ForeColor = Color.Red;
            } else {
                firstNameCheckLabel.Visible = false;
            }

            if ( secondNameTextBox.Text == String.Empty ) {
                secondNameCheckLabel.Visible = true;
                secondNameCheckLabel.Text = "Не может быть пустым!";
                secondNameCheckLabel.ForeColor = Color.Red;
            } else {
                secondNameCheckLabel.Visible = false;
            }
        }



        private void firstNameTextBox_TextChanged(object sender, EventArgs e)
        {
            CheckFirstSecondName();
        }


        private void secondNameTextBox_TextChanged(object sender, EventArgs e)
        {
            CheckFirstSecondName();
        }


        private void firstNameTextBox_Click(object sender, EventArgs e)
        {
            MaxRepeatPswdTextBoxTextBox();
            CheckMaxPswdTextBox();
            CheckMaxLoginTextBox();
            CheckFirstSecondName();
        }


        private void secondNameTextBox_Click(object sender, EventArgs e)
        {
            MaxRepeatPswdTextBoxTextBox();
            CheckMaxPswdTextBox();
            CheckMaxLoginTextBox();
            CheckFirstSecondName();
        }

        private void maxRepeatPswdTextBox_Click(object sender, EventArgs e)
        {
            MaxRepeatPswdTextBoxTextBox();
            CheckMaxPswdTextBox();
            CheckMaxLoginTextBox();
            CheckFirstSecondName();
        }


        private void maxPswdTextBox_Click(object sender, EventArgs e)
        {
            MaxRepeatPswdTextBoxTextBox();
            CheckMaxPswdTextBox();
            CheckMaxLoginTextBox();
            CheckFirstSecondName();
        }

        private void maxRepeatPswdTextBox_TextChanged(object sender, EventArgs e)
        {
            if ( maxRepeatPswdTextBox.Text != maxPswdTextBox.Text ) {
                repeatPasswordCheckLabel.Text = "Не совпадают!";
                repeatPasswordCheckLabel.Visible = true;
                repeatPasswordCheckLabel.ForeColor = Color.Red;
            } else {
                repeatPasswordCheckLabel.Text = "Совпадают!";
                repeatPasswordCheckLabel.Visible = true;
                repeatPasswordCheckLabel.ForeColor = Color.Green;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
