﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SecurityLabs
{
    class DataBaseAccess
    {
        private SecurityLabDataContext _securityLab;

        public DataBaseAccess()
        {
            _securityLab = new SecurityLabDataContext();
        }


        public String GetSalt(String usrName)
        {
            var usr = from elem in _securityLab.Users
                      where elem.UserName == usrName
                      select elem;

            if ( usr.Count() <= 0 )
                return null;

            String res = usr.First().FirstName +
                usr.First().SecondName;

            return res;
        }


        public String GetHashedPassword(String usrName)
        {
            var usr = from elem in _securityLab.Users
                      where elem.UserName == usrName
                      select elem.HashedPassword;

            if ( usr.Any() )
                return usr.First();

            return null;
        }


        public bool Auth(String usrName, String pswd)
        {
            String salt = String.Empty;
            salt = GetSalt(usrName);

            if ( salt == null ) {
                Console.WriteLine("Debug: there is no such user");
                return false;
            }

            String rawSalted = salt + pswd.Trim();

            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] bytes = sha1.ComputeHash(
                Encoding.Unicode.GetBytes(rawSalted));
            String hashedPswd = Convert.ToBase64String(bytes);

            return hashedPswd == GetHashedPassword(usrName);
        }


        public bool RegisterUser(String firstName, String secondName,
            String login, String pswd)
        {
            String rawSalted = firstName + secondName + pswd;
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] bytes = sha1.ComputeHash(
                Encoding.Unicode.GetBytes(rawSalted));
            String hashedPswd = Convert.ToBase64String(bytes);

            User user = new User {
                FirstName = firstName,
                SecondName = secondName,
                UserName = login,
                HashedPassword = hashedPswd
            };

            _securityLab.Users.InsertOnSubmit(user);
            try {
                _securityLab.SubmitChanges();
            } catch ( Exception e ) {
                return false;
            }

            return true;
        }


        public bool ContainsLogin(String userName)
        {
            var usr = from elem in _securityLab.Users
                      where elem.UserName == userName
                      select elem;

            if ( usr.Count() <= 0 )
                return false;

            return true;
        }
    }
}
