﻿namespace SecurityLabs
{
    partial class LogInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.topPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nameLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.minLogInPanel = new System.Windows.Forms.Panel();
            this.minPswdTextBox = new System.Windows.Forms.TextBox();
            this.minLoginTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.logInButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.separatorPanel = new System.Windows.Forms.Panel();
            this.parButton = new System.Windows.Forms.Button();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.maxLogInPanel = new System.Windows.Forms.Panel();
            this.repeatPasswordCheckLabel = new System.Windows.Forms.Label();
            this.maxPswdCheckLabel = new System.Windows.Forms.Label();
            this.maxLoginCheckLabel = new System.Windows.Forms.Label();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.maxPswdTextBox = new System.Windows.Forms.TextBox();
            this.maxLoginTextBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.secondNameTextBox = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.maxRepeatPswdTextBox = new System.Windows.Forms.TextBox();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.secondNameCheckLabel = new System.Windows.Forms.Label();
            this.firstNameCheckLabel = new System.Windows.Forms.Label();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.minLogInPanel.SuspendLayout();
            this.contentPanel.SuspendLayout();
            this.maxLogInPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.Transparent;
            this.topPanel.Controls.Add(this.panel1);
            this.topPanel.Controls.Add(this.nameLabel);
            this.topPanel.Controls.Add(this.pictureBox1);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Margin = new System.Windows.Forms.Padding(0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(655, 123);
            this.topPanel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 123);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(635, 652);
            this.panel1.TabIndex = 0;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameLabel.ForeColor = System.Drawing.Color.Black;
            this.nameLabel.Location = new System.Drawing.Point(187, 14);
            this.nameLabel.Margin = new System.Windows.Forms.Padding(0);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(339, 95);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = "Planner";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SecurityLabs.Properties.Resources.paperPlane;
            this.pictureBox1.Location = new System.Drawing.Point(12, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(160, 123);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // minLogInPanel
            // 
            this.minLogInPanel.BackColor = System.Drawing.Color.Transparent;
            this.minLogInPanel.Controls.Add(this.minPswdTextBox);
            this.minLogInPanel.Controls.Add(this.minLoginTextBox);
            this.minLogInPanel.Controls.Add(this.label2);
            this.minLogInPanel.Controls.Add(this.label1);
            this.minLogInPanel.Location = new System.Drawing.Point(0, 0);
            this.minLogInPanel.Margin = new System.Windows.Forms.Padding(0);
            this.minLogInPanel.Name = "minLogInPanel";
            this.minLogInPanel.Size = new System.Drawing.Size(635, 130);
            this.minLogInPanel.TabIndex = 1;
            // 
            // minPswdTextBox
            // 
            this.minPswdTextBox.Location = new System.Drawing.Point(189, 80);
            this.minPswdTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.minPswdTextBox.Name = "minPswdTextBox";
            this.minPswdTextBox.Size = new System.Drawing.Size(233, 22);
            this.minPswdTextBox.TabIndex = 3;
            this.minPswdTextBox.UseSystemPasswordChar = true;
            // 
            // minLoginTextBox
            // 
            this.minLoginTextBox.Location = new System.Drawing.Point(189, 28);
            this.minLoginTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.minLoginTextBox.Name = "minLoginTextBox";
            this.minLoginTextBox.Size = new System.Drawing.Size(233, 22);
            this.minLoginTextBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(61, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Логин:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(61, 81);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Пароль:";
            // 
            // logInButton
            // 
            this.logInButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logInButton.Location = new System.Drawing.Point(257, 27);
            this.logInButton.Margin = new System.Windows.Forms.Padding(0, 18, 20, 18);
            this.logInButton.Name = "logInButton";
            this.logInButton.Size = new System.Drawing.Size(104, 30);
            this.logInButton.TabIndex = 4;
            this.logInButton.Text = "Вход";
            this.logInButton.UseVisualStyleBackColor = true;
            this.logInButton.Click += new System.EventHandler(this.logInButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(381, 27);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(0, 18, 20, 18);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(95, 30);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // separatorPanel
            // 
            this.separatorPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.separatorPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.separatorPanel.Location = new System.Drawing.Point(20, 4);
            this.separatorPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.separatorPanel.Name = "separatorPanel";
            this.separatorPanel.Size = new System.Drawing.Size(619, 1);
            this.separatorPanel.TabIndex = 6;
            // 
            // parButton
            // 
            this.parButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.parButton.Location = new System.Drawing.Point(496, 27);
            this.parButton.Margin = new System.Windows.Forms.Padding(0, 18, 20, 18);
            this.parButton.Name = "parButton";
            this.parButton.Size = new System.Drawing.Size(127, 30);
            this.parButton.TabIndex = 7;
            this.parButton.Text = "Регистрация >>";
            this.parButton.UseVisualStyleBackColor = true;
            this.parButton.Click += new System.EventHandler(this.paramsButton_Click);
            // 
            // contentPanel
            // 
            this.contentPanel.BackColor = System.Drawing.Color.Transparent;
            this.contentPanel.Controls.Add(this.maxLogInPanel);
            this.contentPanel.Controls.Add(this.minLogInPanel);
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(0, 123);
            this.contentPanel.Margin = new System.Windows.Forms.Padding(0);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(655, 610);
            this.contentPanel.TabIndex = 2;
            // 
            // maxLogInPanel
            // 
            this.maxLogInPanel.Controls.Add(this.firstNameCheckLabel);
            this.maxLogInPanel.Controls.Add(this.secondNameCheckLabel);
            this.maxLogInPanel.Controls.Add(this.repeatPasswordCheckLabel);
            this.maxLogInPanel.Controls.Add(this.maxPswdCheckLabel);
            this.maxLogInPanel.Controls.Add(this.maxLoginCheckLabel);
            this.maxLogInPanel.Controls.Add(this.firstNameTextBox);
            this.maxLogInPanel.Controls.Add(this.label8);
            this.maxLogInPanel.Controls.Add(this.label7);
            this.maxLogInPanel.Controls.Add(this.maxPswdTextBox);
            this.maxLogInPanel.Controls.Add(this.maxLoginTextBox);
            this.maxLogInPanel.Controls.Add(this.button2);
            this.maxLogInPanel.Controls.Add(this.secondNameTextBox);
            this.maxLogInPanel.Controls.Add(this.panel3);
            this.maxLogInPanel.Controls.Add(this.button3);
            this.maxLogInPanel.Controls.Add(this.label4);
            this.maxLogInPanel.Controls.Add(this.button4);
            this.maxLogInPanel.Controls.Add(this.label5);
            this.maxLogInPanel.Controls.Add(this.label3);
            this.maxLogInPanel.Controls.Add(this.maxRepeatPswdTextBox);
            this.maxLogInPanel.Location = new System.Drawing.Point(0, 0);
            this.maxLogInPanel.Margin = new System.Windows.Forms.Padding(0);
            this.maxLogInPanel.Name = "maxLogInPanel";
            this.maxLogInPanel.Size = new System.Drawing.Size(655, 263);
            this.maxLogInPanel.TabIndex = 18;
            this.maxLogInPanel.Visible = false;
            // 
            // repeatPasswordCheckLabel
            // 
            this.repeatPasswordCheckLabel.AutoSize = true;
            this.repeatPasswordCheckLabel.Location = new System.Drawing.Point(432, 210);
            this.repeatPasswordCheckLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.repeatPasswordCheckLabel.Name = "repeatPasswordCheckLabel";
            this.repeatPasswordCheckLabel.Size = new System.Drawing.Size(46, 17);
            this.repeatPasswordCheckLabel.TabIndex = 25;
            this.repeatPasswordCheckLabel.Text = "label6";
            this.repeatPasswordCheckLabel.Visible = false;
            // 
            // maxPswdCheckLabel
            // 
            this.maxPswdCheckLabel.AutoSize = true;
            this.maxPswdCheckLabel.Location = new System.Drawing.Point(432, 165);
            this.maxPswdCheckLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.maxPswdCheckLabel.Name = "maxPswdCheckLabel";
            this.maxPswdCheckLabel.Size = new System.Drawing.Size(46, 17);
            this.maxPswdCheckLabel.TabIndex = 24;
            this.maxPswdCheckLabel.Text = "label6";
            this.maxPswdCheckLabel.Visible = false;
            // 
            // maxLoginCheckLabel
            // 
            this.maxLoginCheckLabel.AutoSize = true;
            this.maxLoginCheckLabel.Location = new System.Drawing.Point(432, 119);
            this.maxLoginCheckLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.maxLoginCheckLabel.Name = "maxLoginCheckLabel";
            this.maxLoginCheckLabel.Size = new System.Drawing.Size(46, 17);
            this.maxLoginCheckLabel.TabIndex = 23;
            this.maxLoginCheckLabel.Text = "label6";
            this.maxLoginCheckLabel.Visible = false;
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.firstNameTextBox.Location = new System.Drawing.Point(205, 25);
            this.firstNameTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(217, 22);
            this.firstNameTextBox.TabIndex = 22;
            this.firstNameTextBox.Click += new System.EventHandler(this.firstNameTextBox_Click);
            this.firstNameTextBox.TextChanged += new System.EventHandler(this.firstNameTextBox_TextChanged);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(49, 71);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 20);
            this.label8.TabIndex = 21;
            this.label8.Text = "Фамилия:";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(49, 26);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 20);
            this.label7.TabIndex = 20;
            this.label7.Text = "Имя:";
            // 
            // maxPswdTextBox
            // 
            this.maxPswdTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.maxPswdTextBox.Location = new System.Drawing.Point(204, 161);
            this.maxPswdTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.maxPswdTextBox.Name = "maxPswdTextBox";
            this.maxPswdTextBox.Size = new System.Drawing.Size(219, 22);
            this.maxPswdTextBox.TabIndex = 19;
            this.maxPswdTextBox.UseSystemPasswordChar = true;
            this.maxPswdTextBox.Click += new System.EventHandler(this.maxPswdTextBox_Click);
            this.maxPswdTextBox.TextChanged += new System.EventHandler(this.maxPswdTextBox_TextChanged);
            // 
            // maxLoginTextBox
            // 
            this.maxLoginTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.maxLoginTextBox.Location = new System.Drawing.Point(205, 116);
            this.maxLoginTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 25);
            this.maxLoginTextBox.Name = "maxLoginTextBox";
            this.maxLoginTextBox.Size = new System.Drawing.Size(217, 22);
            this.maxLoginTextBox.TabIndex = 10;
            this.maxLoginTextBox.TextChanged += new System.EventHandler(this.maxLoginTextBox_TextChanged);
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Location = new System.Drawing.Point(480, 411);
            this.button2.Margin = new System.Windows.Forms.Padding(0, 6, 20, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(143, 30);
            this.button2.TabIndex = 15;
            this.button2.Text = "Параметры >>";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // secondNameTextBox
            // 
            this.secondNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.secondNameTextBox.Location = new System.Drawing.Point(204, 71);
            this.secondNameTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.secondNameTextBox.Name = "secondNameTextBox";
            this.secondNameTextBox.Size = new System.Drawing.Size(219, 22);
            this.secondNameTextBox.TabIndex = 17;
            this.secondNameTextBox.Click += new System.EventHandler(this.secondNameTextBox_Click);
            this.secondNameTextBox.TextChanged += new System.EventHandler(this.secondNameTextBox_TextChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.Location = new System.Drawing.Point(16, 594);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(607, 1);
            this.panel3.TabIndex = 14;
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button3.Location = new System.Drawing.Point(351, 411);
            this.button3.Margin = new System.Windows.Forms.Padding(0, 6, 20, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(109, 30);
            this.button3.TabIndex = 13;
            this.button3.Text = "Отмена";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(49, 162);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Пароль:";
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button4.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button4.Location = new System.Drawing.Point(221, 411);
            this.button4.Margin = new System.Windows.Forms.Padding(0, 6, 20, 6);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(109, 30);
            this.button4.TabIndex = 12;
            this.button4.Text = "Вход";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(49, 208);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Парль еще раз:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(49, 117);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Логин:";
            // 
            // maxRepeatPswdTextBox
            // 
            this.maxRepeatPswdTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.maxRepeatPswdTextBox.Location = new System.Drawing.Point(205, 207);
            this.maxRepeatPswdTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 25);
            this.maxRepeatPswdTextBox.Name = "maxRepeatPswdTextBox";
            this.maxRepeatPswdTextBox.Size = new System.Drawing.Size(217, 22);
            this.maxRepeatPswdTextBox.TabIndex = 11;
            this.maxRepeatPswdTextBox.UseSystemPasswordChar = true;
            this.maxRepeatPswdTextBox.Click += new System.EventHandler(this.maxRepeatPswdTextBox_Click);
            this.maxRepeatPswdTextBox.TextChanged += new System.EventHandler(this.maxRepeatPswdTextBox_TextChanged);
            // 
            // bottomPanel
            // 
            this.bottomPanel.BackColor = System.Drawing.Color.Transparent;
            this.bottomPanel.Controls.Add(this.parButton);
            this.bottomPanel.Controls.Add(this.separatorPanel);
            this.bottomPanel.Controls.Add(this.logInButton);
            this.bottomPanel.Controls.Add(this.cancelButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 658);
            this.bottomPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(655, 75);
            this.bottomPanel.TabIndex = 3;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 0;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            // 
            // secondNameCheckLabel
            // 
            this.secondNameCheckLabel.AutoSize = true;
            this.secondNameCheckLabel.Location = new System.Drawing.Point(432, 74);
            this.secondNameCheckLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.secondNameCheckLabel.Name = "secondNameCheckLabel";
            this.secondNameCheckLabel.Size = new System.Drawing.Size(46, 17);
            this.secondNameCheckLabel.TabIndex = 26;
            this.secondNameCheckLabel.Text = "label6";
            this.secondNameCheckLabel.Visible = false;
            // 
            // firstNameCheckLabel
            // 
            this.firstNameCheckLabel.AutoSize = true;
            this.firstNameCheckLabel.Location = new System.Drawing.Point(432, 28);
            this.firstNameCheckLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.firstNameCheckLabel.Name = "firstNameCheckLabel";
            this.firstNameCheckLabel.Size = new System.Drawing.Size(142, 17);
            this.firstNameCheckLabel.TabIndex = 27;
            this.firstNameCheckLabel.Text = "firstNameCheckLabel";
            this.firstNameCheckLabel.Visible = false;
            // 
            // LogInForm
            // 
            this.AcceptButton = this.logInButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(655, 733);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.contentPanel);
            this.Controls.Add(this.topPanel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LogInForm";
            this.Text = "Planner: Авторизация";
            this.Load += new System.EventHandler(this.LogInForm_Load);
            this.Resize += new System.EventHandler(this.LogInForm_Resize);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.minLogInPanel.ResumeLayout(false);
            this.minLogInPanel.PerformLayout();
            this.contentPanel.ResumeLayout(false);
            this.maxLogInPanel.ResumeLayout(false);
            this.maxLogInPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel minLogInPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button logInButton;
        private System.Windows.Forms.TextBox minPswdTextBox;
        private System.Windows.Forms.TextBox minLoginTextBox;
        private System.Windows.Forms.Panel separatorPanel;
        private System.Windows.Forms.Button parButton;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox maxRepeatPswdTextBox;
        private System.Windows.Forms.TextBox maxLoginTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox secondNameTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel maxLogInPanel;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.TextBox maxPswdTextBox;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label repeatPasswordCheckLabel;
        private System.Windows.Forms.Label maxPswdCheckLabel;
        private System.Windows.Forms.Label maxLoginCheckLabel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label firstNameCheckLabel;
        private System.Windows.Forms.Label secondNameCheckLabel;
    }
}